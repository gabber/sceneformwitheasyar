//================================================================================================================================
//
// Copyright (c) 2015-2020 VisionStar Information Technology (Shanghai) Co., Ltd. All Rights Reserved.
// EasyAR is the registered trademark or trademark of VisionStar Information Technology (Shanghai) Co., Ltd in China
// and other countries for the augmented reality technology developed by VisionStar Information Technology (Shanghai) Co., Ltd.
//
//================================================================================================================================
package cn.easyar.samples.helloarsurfacetracking

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.graphics.PixelFormat
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.annotation.RequiresApi
import cn.easyar.CameraDevice
import cn.easyar.Engine
import cn.easyar.SurfaceTracker
import cn.easyar.samples.helloarsurfacetracking.ARActivity
import cn.easyar.samples.helloarsurfacetracking.nodes.DragTransformableNode
import com.google.ar.core.exceptions.CameraNotAvailableException
import com.google.ar.sceneform.HitTestResult
import com.google.ar.sceneform.rendering.ModelRenderable
import com.google.ar.sceneform.ux.FootprintSelectionVisualizer
import com.google.ar.sceneform.ux.TransformationSystem
import kotlinx.android.synthetic.main.activity_ar.*
import java.util.*
import java.util.concurrent.CompletionException

@RequiresApi(Build.VERSION_CODES.N)
class ARActivity : Activity() {

    private var glView: GLView? = null
    var localModel = "model.sfb"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ar)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
            WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
        )
        if (!Engine.initialize(this, key)) {
            Log.e("HelloAR", "Initialization Failed.")
            Toast.makeText(this@ARActivity, Engine.errorMessage(), Toast.LENGTH_LONG).show()
            return
        }
        if (!CameraDevice.isAvailable()) {
            Toast.makeText(this@ARActivity, "CameraDevice not available.", Toast.LENGTH_LONG).show()
            return
        }
        if (!SurfaceTracker.isAvailable()) {
            Toast.makeText(this@ARActivity, "SurfaceTracker not available.", Toast.LENGTH_LONG)
                .show()
            return
        }
        glView = GLView(this).apply {
//            setZOrderOnTop(true)
        }
        requestCameraPermission(object : PermissionCallback {
            override fun onSuccess() {
                val preview = findViewById<View>(R.id.preview) as ViewGroup
                preview.addView(
                    glView,
                    ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                    )
                )
            }

            override fun onFailure() {}
        })

        sceneView.setZOrderOnTop(true)
        val sfhTrackHolder = sceneView.holder
        sfhTrackHolder.setFormat(PixelFormat.TRANSPARENT)
        sceneView.setZOrderMediaOverlay(true)

        renderLocalObject()
    }

    private interface PermissionCallback {
        fun onSuccess()
        fun onFailure()
    }

    private val permissionCallbacks = HashMap<Int, PermissionCallback>()
    private var permissionRequestCodeSerial = 0

    private fun requestCameraPermission(callback: PermissionCallback) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                val requestCode = permissionRequestCodeSerial
                permissionRequestCodeSerial += 1
                permissionCallbacks[requestCode] = callback
                requestPermissions(arrayOf(Manifest.permission.CAMERA), requestCode)
            } else {
                callback.onSuccess()
            }
        } else {
            callback.onSuccess()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (permissionCallbacks.containsKey(requestCode)) {
            val callback = permissionCallbacks[requestCode]
            permissionCallbacks.remove(requestCode)
            var executed = false
            for (result in grantResults) {
                if (result != PackageManager.PERMISSION_GRANTED) {
                    executed = true
                    callback!!.onFailure()
                }
            }
            if (!executed) {
                callback!!.onSuccess()
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onResume() {
        super.onResume()
        if (glView != null) {
            glView!!.onResume()
        }
        try {
            sceneView.resume()
        } catch (e: CameraNotAvailableException) {
            e.printStackTrace()
        }
    }

    override fun onPause() {
        if (glView != null) {
            glView!!.onPause()
        }
        sceneView.pause()
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            sceneView.destroy()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun renderLocalObject() {
        ModelRenderable.builder()
            .setSource(this, Uri.parse(localModel))
            .setRegistryId(localModel)
            .build()
            .thenAccept { modelRenderable: ModelRenderable ->
                addNodeToScene(modelRenderable)
            }
            .exceptionally { throwable: Throwable? ->
                val message: String = if (throwable is CompletionException) {
                    "Internet is not working"
                } else {
                    "Can't load Model"
                }
                val mainHandler = Handler(Looper.getMainLooper())
                val finalMessage: String = message
                val myRunnable = Runnable {
                    AlertDialog.Builder(this)
                        .setTitle("Error")
                        .setMessage(finalMessage + "")
                        .setPositiveButton("Retry") { dialogInterface: DialogInterface, _: Int ->
                            renderLocalObject()
                            dialogInterface.dismiss()
                        }
                        .setNegativeButton("Cancel") { dialogInterface, _ -> dialogInterface.dismiss() }
                        .show()
                }
                mainHandler.post(myRunnable)
                null
            }
    }


    private fun addNodeToScene(model: ModelRenderable) {
        if (sceneView != null) {
            val transformationSystem = makeTransformationSystem()
            val dragTransformableNode = DragTransformableNode(1f, transformationSystem)
            dragTransformableNode.renderable = model
            sceneView.scene.addChild(dragTransformableNode)
            dragTransformableNode.select()
            sceneView.scene
                .addOnPeekTouchListener { hitTestResult: HitTestResult?, motionEvent: MotionEvent? ->
                    transformationSystem.onTouch(
                        hitTestResult,
                        motionEvent
                    )
                }
        }
    }

    private fun makeTransformationSystem(): TransformationSystem {
        val footprintSelectionVisualizer = FootprintSelectionVisualizer()
        return TransformationSystem(resources.displayMetrics, footprintSelectionVisualizer)
    }


    object Statics {
        var EXTRA_MODEL_TYPE = "modelType"
    }

    companion object {
        /*
    * Steps to create the key for this sample:
    *  1. login www.easyar.com
    *  2. create app with
    *      Name: HelloARSurfaceTracking
    *      Package Name: cn.easyar.samples.helloarsurfacetracking
    *  3. find the created item in the list and show key
    *  4. set key string bellow
    */
        private const val key =
            "SIyOf0yflmNU+Rh6dSCDqUltRf7oR9eYzbKmBHi+uFRMrr5JeKOpBDfvvEhpv7JCaLvqETqNuktspLEIbqKwBCHvsEd+ubhURqikb2nv5xch77FPbqizVWi+/xxWtv9EeKO5SmiEuVUv94Z7Ie+rR3+kvEh5vv8cVu++SWCgqEhkuaQEUOH/VmGsqUBiv7BVL/eGBHqks0Jiuq4EIe+wR27vgAovoLJCeKG4VS/3hgR+qLNVaOOUS2yquHJ/rL5NZKO6BCHvrkNjvrgITqGyU2mfuEViqrNPeaSySC/h/1Voo65DI5+4RWK/uU9jqv8KL764SH6o82lvp7hFeZmvR26mtEhq7/EEfqizVWjjjlN/q7xFaJmvR26mtEhq7/EEfqizVWjjjlZsv65DXr28UmSssWtsvf8KL764SH6o82tiubRJY5mvR26mtEhq7/EEfqizVWjjmUNjvrh1faypT2yhkEd97/EEfqizVWjjnmdJma9Hbqa0SGrvgAovqKVWZL+4cmSguHV5rLBWL/ezU2Gh8QRkvpFJbqyxBDervEp+qKAKdu+/U2OpsUNEqa4EN5b/RWPjuEd+tLxUI768S32huFUjpbhKYaK8VH64r0BsrrhSf6y+TWSjugRQ4f9QbL+0R2O5rgQ3lv9FYqCwU2OkqV8vkPEEfaG8Umuir0t+7+d9L6yzQn+itEIvkPEEYKK5U2GorgQ3lv9VaKOuQyOEsEdqqIlUbK62T2Oq/wovvrhIfqjzZWGiqEJfqL5JaqO0UmSiswQh765DY764CF+ovkl/qbRIau/xBH6os1Vo45JEZ6i+Ulm/vEVmpLNBL+H/VWijrkMjnqhUa6y+Q1m/vEVmpLNBL+H/VWijrkMjnq1Hf764dX2sqU9soZBHfe/xBH6os1Vo45BJeaSySFm/vEVmpLNBL+H/VWijrkMjibhIfqiOVmy5tEdhgLxWL+H/VWijrkMjjpxiWb+8RWaks0EvkPEEaLWtT3+oiU9gqI5SbKCtBDejqEph4f9PfoGyRWyh/xxrrLFVaLDxXS+vqEhpobhvab7/HFbv/3sh76tHf6S8SHm+/xxW775JYKCoSGS5pARQ4f9WYaypQGK/sFUv94YEZKKuBFDh/0tiqahKaL7/HFbvrkNjvrgIRKC8QWiZr0duprRIau/xBH6os1Vo455KYri5dGiuskFjpKlPYqP/Ci++uEh+qPN0aK6yVGmks0Ev4f9VaKOuQyOCv0xorqlyf6y+TWSjugQh765DY764CF64r0Bsrrhyf6y+TWSjugQh765DY764CF69vFR+qI5WbLm0R2GAvFYv4f9VaKOuQyOAslJkorNyf6y+TWSjugQh765DY764CEmos1Vonq1HeaS8SkCsrQQh765DY764CE6MmXJ/rL5NZKO6BFDh/0N1vbRUaJm0S2ieqUdgvf8cY7ixSiHvtFVBor5HYe/nQGyhrkNwkKAmaolXTaYbx3wUdkgdgzQRH7QPE7LHNTeSbkJ66UyuHv065N79PWAUNL8WFTBKmTXvuWmFFL/czTi8jjCPoBxhZrGtsnG9WGGuzH3luxwYVMfBuHkl13RQhXa4dcc8seTR28vUVM1Ms1r+SUhNZ0JRKP+Pv2lOvkuileQW2S8G5KqHC2xt7wVCdwY4PYJdj1GPeu52fBxR5TXcgvjxCMYmVV4OV9o47rw5KQr0qJCgOr+/AZvnZCrqYIArLryGrjj8qse+L3X7sb6N4Ww2CDO0CRXcV4JNp0PoCH6593FjEfqc6sXmcLfMSuP4l+d61NpxJvy/3ZSSbsXX0zYNzd0m"
    }
}